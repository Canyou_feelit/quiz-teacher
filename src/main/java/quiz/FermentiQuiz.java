package quiz;

import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class FermentiQuiz extends Quiz {

    public FermentiQuiz() {
        super();
    }

    @Override
    protected int multipleQuestionNo() {
        return 106;
    }
    @Override
    protected int getMaxQuestions() {
        return this.questions.values().size();
    }

    @Override
    protected int getMinQuestions() {
        return 5;
    }

    @Override
    protected InputStream getFilename() {
        //return this.getClass().getResource("/fermenti-quiz-correct.txt").getFile();
        //return System.class.getResource("/fermenti-quiz-correct.txt").getFile();
        return this.getClass().getResourceAsStream("/fermenti-quiz-correct.txt");
    }

    @Override
    protected void parseData(List<String> data) {
        Integer currentQuestion = 0;
        for (String str : data) {
            if (str.contains(":")) {

                currentQuestion = Integer.valueOf(str.split(" ")[0].replace(".", ""));
                List<String> tmp = new ArrayList<>();
                tmp.add(StringUtils.substringAfter(str, "."));

                questions.put(currentQuestion, tmp);
            }
            // questions
            else if (str.split(",").length >= 1 && StringUtils.containsAny(str, "АБВГД")
                    && !StringUtils.contains(str, ".")) {
                List<String> tmp = correctAnswers.containsKey(currentQuestion) ?
                        correctAnswers.get(currentQuestion) : new ArrayList<>();

                tmp.addAll(
                        Stream.of(str.split(",")).
                                map(StringUtils::trim)
                                .collect(toList())
                );
                correctAnswers.put(currentQuestion, tmp);
            }
            // answers
            else {
                List<String> tmp = questions.containsKey(currentQuestion) ?
                        questions.get(currentQuestion) : new ArrayList<>();

                tmp.add(str);
                questions.put(currentQuestion, tmp);
            }
        }
        //System.out.println(questions);
    }
}

        /* STREAM_API */
        /*
        // prepare questions
        this.questions = data.stream()
                .filter(str -> str.contains(":"))
                .collect(toMap(
                        str -> Integer.valueOf(StringUtils.substringBefore(str, ".")),
                        str -> StringUtils.substringAfter(str, ".")
                ));

        this.correctAnswers = data.stream()
                .filter(str -> (str.contains(",") && !str.contains(".")) || str.length() == 1)
                .collect(toList());

       this.variants = new ArrayList<>();
       String tmp = "";
       for (String element : data) {

            if (StringUtils.containsAny(element,"АБВГД") && element.contains(".")) {
                if (StringUtils.containsAny(StringUtils.substringBefore(element.split(" ")[0],"."),
                        "АБВГ")) {
                    tmp += element + "\n";
                }
                else if(StringUtils.containsAny(StringUtils.substringBefore(element.split(" ")[0],"."),
                        "Д")) {
                    tmp += element + "\n";
                    variants.add(tmp);
                    tmp = "";
                }
            }
        }

    }
*/
    /*
        // 1 - question; 2 - 6 answers; 7 - right answer
        while (readLine != null) {
            // Get questions
            Integer questionNumber = Integer.valueOf(readLine.split(" ")[0].replaceAll("\\.", ""));
            questions.putAll(performQuestions(readLine));

            // Get Answers
            int answersNum = questionNumber >= 55 ? 8 : 5;

            List<Map<String, String>> answrs = new ArrayList<>();
            for (int i = 1; i <= answersNum; i++) {
                readLine = br.readLine();
                answrs.add(performAnswers(readLine));
            }
            answers.put(questionNumber, answrs);

            // Get right Answer
            readLine = br.readLine();

            List<String> rightAnswer = new ArrayList<>();
            rightAnswer.addAll(Stream.of(readLine.toUpperCase().trim().split(",")).collect(Collectors.toList()));
            rightAnswer.replaceAll(String::trim);
            rightAnswers.put(questionNumber, new ArrayList(rightAnswer));

            readLine = br.readLine();
        }
        this.questionsNumbers = new ArrayList<>(questions.keySet());
        */


































