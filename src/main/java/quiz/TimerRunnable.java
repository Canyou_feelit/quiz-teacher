package quiz;

import java.sql.Time;

public class TimerRunnable implements Runnable {

    Thread thr;
    int ms;

    Quiz quiz;

    TimerRunnable(Quiz quiz, int ms) {
        this.ms = ms;
        this.quiz = quiz;

        System.out.println("На тест дается " + ms / 60 + " минут(ы/а). Время пошло, удачи.");
        thr = new Thread(this, "TimerStart");
        thr.start();
    }

    @Override
    public void run() {
        try {
            for (int i = 1; i < ms; i++) {
                if (quiz.isTestEnded())
                    thr.interrupt();
                Thread.sleep(1000);
                if (i % 180 == 0) {
                    int existTime = (ms - i);
                    if (existTime > 60)
                        existTime /= 60;
                    System.out.print("\rОсталось " + existTime + (existTime < 60 ? " минут(ы)" : " секунд(ы)") + "> ");
                }
            }
            quiz.setByTimer(true);
            quiz.getResults();
        } catch (InterruptedException intEx) {
            System.out.println(intEx.getMessage());
        }
    }
}
