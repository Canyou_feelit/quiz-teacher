import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
/*
public class BelkiQuiz extends quiz.Quiz {

    public BelkiQuiz() {
        this.reset();
    }

    public void reset() {
        this.byTime = false;
        this.testEnded = false;
        this.numCorrectAnswers = 0;
        this.numOfQuestions = 72;
        this.currentQuestionNum = 0;
    }

    @Override
    protected void parseData(List<String> data) {
        //this.
    }
/*    public void parseFile() throws FileNotFoundException {
        final String filename = "/home/dee/dev/projects/java/QuizBiology/src/fermenti-quiz.txt";

        BufferedReader br = new BufferedReader(new FileReader(filename));
        String readLine = br.readLine();

        // 1 - question; 2 - 6 answers; 7 - right answer
        while (readLine != null) {
            // Get questions
            Integer questionNumber = Integer.valueOf(readLine.split(" ")[0].replaceAll("\\.", ""));
            questions.putAll(performQuestions(readLine));

            // Get Answers
            int answersNum = questionNumber >= 55 ? 8 : 5;

            List<Map<String, String>> answrs = new ArrayList<>();
            for (int i = 1; i <= answersNum; i++) {
                readLine = br.readLine();
                answrs.add(performAnswers(readLine));
            }
            answers.put(questionNumber, answrs);

            // Get right Answer
            readLine = br.readLine();

            /*
              public Map<String, List<String>> someName() throws Exception {
        List<String> strings = IOUtils.readLines(new FileInputStream("test_file.txt"), Charset.forName("UTF-8"));
        return strings.stream()
                .collect(Collectors.toMap(
                        str -> StringUtils.substringBefore(str, ":"),
                        str -> Arrays.stream(StringUtils.substringAfter(str, ":").split(" "))
                                        .filter(StringUtils::isNotEmpty)
                                        .map(StringUtils::trim)
                                        .collect(toList())));
              }

            List<String> rightAnswer = new ArrayList<>();
            rightAnswer.addAll(Stream.of(readLine.toUpperCase().trim().split(",")).collect(Collectors.toList()));
            rightAnswer.replaceAll(String::trim);
            rightAnswers.put(questionNumber, new ArrayList(rightAnswer));

            readLine = br.readLine();
        }
        this.questionsNumbers = new ArrayList<>(questions.keySet());
    }

    public void startRandom() throws NoDataException {
        if(!canBegin()) throw new NoDataException("Вопросов еще нет.");

        Collections.shuffle(questionsNumbers);
        this.start();
    }
/*
    private boolean canBegin() {
        return this.answers.size() == 0 || this.questions.size() == 0 ? false : true;
    }




    public void start() throws NoDataException {
        if (!canBegin()) throw new NoDataException("Вопросов еще нет.");

        //System.out.println("Если хотите закончить тест, введите 'конец' (Прогресс не сохранится, ждите в будущем)");

        System.out.println("Количество вопросов в тесте сколько хотите? Минимум 10, максимум 72");
        String numOfQuestions = inputReader.nextLine();
        while (!numOfQuestions.matches("[1-6][0-9]|(72)")) {
        //while (!numOfQuestions.matches(".*\\d+.*")) {
            System.out.println("Нужно ввести число от 10 до 72, сколько тестов надо");
            numOfQuestions = inputReader.nextLine();
        }


        // 1 minute on 1 question
        System.out.println("Прежде чем мы начнем, какое время хочешь для теста(в минутах)? (максимум " +
                numOfQuestions + " минут)");

        String time = inputReader.nextLine();
        String pattern = "я тут бог";
        while (!time.matches(".*\\d+.*") && !time.toLowerCase().equals(pattern)) {
            System.out.println("Нужно ввести число!");
            time = inputReader.nextLine();
        }

        while(Integer.valueOf(time) > Integer.valueOf(numOfQuestions)) {
            System.out.println("Не годится. Не более " + numOfQuestions + " минут");
            time = inputReader.nextLine();
        }

        if (time.toLowerCase().equals(pattern)) {
            System.out.println("Время бесгранично, как для Бога.");
        }
        else {
            TimerRunnable timer = new TimerRunnable(Integer.valueOf(time) * 60);
        }

        this.numOfQuestions = Integer.valueOf(numOfQuestions);
        questionsNumbers = questionsNumbers.subList(0, this.numOfQuestions);
        Collections.shuffle(questionsNumbers);

        while (currentQuestionNum < this.numOfQuestions) {
            if (testEnded)
                break;
            this.nextQuestion();
        }

        getResults();
    }


    /*
    private boolean isCorrectAnswer(String answer, int questionNo) {
        List<String> correctAnswers = rightAnswers.get(questionNo);

        return Arrays.asList(answer.split(" ")).containsAll(correctAnswers) ? true : false;
    }


    private void nextQuestion() {
        Integer questionNo = questionsNumbers.get(currentQuestionNum);
        currentQuestionNum += 1;
        try {
            this.printQuestion(questionNo);
            this.askAnswer(questionNo);
        } catch (NoDataException noData) {
        }
    }

    public void printQuestionWithAnswer(int number) throws NoDataException {
        if (number > answers.size()) throw new NoDataException("Нет такого вопроса");

        this.printQuestion(number);
        this.printCorrectAnswerFor(number);

    }
    public void printCorrectAnswerFor(int number) throws NoDataException {
        if (number > answers.size()) throw new NoDataException("Нет такого вопроса");

        System.out.println("Правильный(е) ответ(ы): ");
        rightAnswers.get(number).forEach(System.out::println);
    }
    public void askAnswer(int number) throws NoDataException {
        if (number > answers.size()) throw new NoDataException("Нет такого вопроса");

        if (rightAnswers.get(number).size() == 1)
            System.out.println("Выберите 1 правильный ответ");
        else {
            System.out.println(number < 54 ?
                    "Введите несколько правильных ответов, через пробел. Например, А В" :
                    "Соедините правильные ответы через пробел по паттерну №-Буква, например, 1-Б 2-В 3-Д");
        }
        System.out.printf("> ");

        String answer = inputReader.nextLine().toUpperCase();


        if (this.isCorrectAnswer(answer, number)) {
            System.out.println(rigghtAnswerReply.get(new Random().nextInt(rigghtAnswerReply.size())));
            this.numCorrectAnswers += 1;
        }
        else {
            System.out.println(wrongAnswerReply.get(new Random().nextInt(wrongAnswerReply.size())));
        }
        System.out.println();
    }
    public void printQuestion(int number) throws NoDataException {
        //questionNo = 25;
        String question = questions.get(number);
        if (question.length() <= 3) {
            question = ":";
        }

        if (number > answers.size()) throw new NoDataException("Нет такого вопроса");
        //System.out.println("Вопрос " + (this.numOfQuestions - questionsNumbers.size()) + " " + question);
        System.out.println("Вопрос " + (currentQuestionNum) + " " + question);
        answers.get(number).forEach((listItem) -> {
            listItem.forEach((key, value) -> {
                System.out.println(key + ": " + value);
            });
        });
    }
    private Map<String, String> performAnswers(String str) {
        String answerCh = str.split(" ")[0].replaceAll("\\.", "").trim();
        String answer = str.substring(str.indexOf(" ") + 1).trim();

        Map<String, String> answers = new HashMap<>();
        answers.put(answerCh, answer);

        return answers;
    }

    private Map<Integer, String> performQuestions(String str) {
        Integer questionNumber = Integer.valueOf(str.split(" ")[0].replaceAll("\\.", ""));
        String question = str.substring(str.indexOf(" ") + 1).trim();

        Map<Integer, String> questions = new HashMap<>();
        questions.put(questionNumber, question);

        return questions;
    }

}
*/
