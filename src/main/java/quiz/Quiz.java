package quiz;

import java.io.*;
import java.util.*;
import static java.util.stream.Collectors.toList;

abstract public class Quiz {

    protected Map<Integer, List<String>> questions;
    protected Map<Integer, List<String>> correctAnswers;

    protected Map<Integer, List<String>> currentQeustions;

    protected int correctAnswersCount;
    protected boolean byTime;

    protected boolean testEnded;
    protected int numOfQuestions;

    abstract protected void parseData(List<String> data);

    abstract protected InputStream getFilename();

    abstract protected int getMaxQuestions();

    abstract protected int getMinQuestions();

    abstract protected int multipleQuestionNo();

    protected Scanner inputReader = new Scanner(System.in);


    final protected List<String> correctAnswerReply = new ArrayList<>(Arrays.asList(
            "Отлично, ответ верный!",
            "10 очков грифиндор!",
            "Абсолютно верно!",
            "Красава!!",
            "Лучшая",
            "В яблокусик!"
    ));
    final protected List<String> wrongAnswerReply = new ArrayList<>(Arrays.asList(
            "Да ты чего, конечно нет!",
            "Ответ неверный!",
            "Снова мимо",
            "Неправильно",
            "Увы, но нет!"
    ));

    public Quiz() {
        this.reset();
    }

    public void reset() {
        this.byTime = false;
        this.testEnded = false;

        this.numOfQuestions = 0;
        this.correctAnswersCount = 0;

        this.questions = new HashMap<>();
        this.correctAnswers = new HashMap<>();
    }

    public void prepareData() {
        BufferedReader br = new BufferedReader(new InputStreamReader(this.getFilename()));

        List<String> readLine = br.lines().collect(toList());


        this.parseData(readLine);
    }

    final protected boolean isCorrectAnswer(String answer, int questionNo) {
        //  List<String> correctAnswers = this.correctAnswers.get(questionNo);

        return Arrays.asList(answer.split(" ")).
                containsAll(this.correctAnswers.get(questionNo)) ? true : false;
        //return this.correctAnswers.get(questionNo).containsAll(Arrays.asList(answer.split(" "))) ? true : false;
    }

    final protected int getNumberOfQestions() {

        System.out.println("Сколько вопросов в тесте хотите?");


        Integer numOfQuestion;
        while (true) {
            System.out.printf("Нужно ввести число от %d до %d, сколько тестов надо\n> ",
                    this.getMinQuestions(), this.getMaxQuestions());

            String input = inputReader.nextLine();
            try {
                numOfQuestion = Integer.valueOf(input.trim());

                if (numOfQuestion > this.getMaxQuestions() ||
                        numOfQuestion < this.getMinQuestions()) {
                    continue;
                }
                return numOfQuestion;
            } catch (NumberFormatException numEx) {
                continue;
            }
        }
    }

    final protected void startTimer() {
        // 1 minute on 1 question
        System.out.println("Прежде чем мы начнем, какое время хочешь для теста(в минутах)? (максимум " +
                this.numOfQuestions + " минут)");

        final String cheatCode = "я тут бог";

        String input;

        while (true) {
            input = inputReader.nextLine().trim().toLowerCase();

            if (input.equals(cheatCode)) {
                System.out.println("Время бесгранично, как для Бога.");
                return;
            }

            if (!input.matches(".*\\d+.*")) {
                System.out.println("Необходимо указать число");
                continue;
            }

            int time = Integer.valueOf(input);
            if (time > this.numOfQuestions) {
                System.out.println("Не годится. Не более " + numOfQuestions + " минут");
                continue;
            }

            TimerRunnable timer = new TimerRunnable(this, time * 60);
            return;
        }
    }

    public boolean isTestEnded() {
        return testEnded;
    }

    protected void setByTimer(boolean time) {
        this.byTime = time;
    }

    public void start() {
        // TODO check if empty question
        this.numOfQuestions = this.getNumberOfQestions();

        this.startTimer();

        Set<Integer> set = new HashSet<>();
        Random rand = new Random();
        int minimum = questions.keySet().iterator().next();

        rand.ints(this.numOfQuestions, minimum, questions.size() - 1)
                .forEach(i -> set.add(i));

        List<Integer> tmpKeys = new ArrayList<>(questions.keySet());
        Collections.shuffle(tmpKeys);
        tmpKeys.subList(0, numOfQuestions).stream()
                .forEach(key -> {
                    questions.get(key).forEach(System.out::println);
                    this.askAnswer(key);
                });

        this.getResults();
    }

    public void askAnswer(Integer number) {
        if (correctAnswers.get(number).size() == 1)
            System.out.println("Выберите 1 правильный ответ");
        else {
            System.out.println(number < this.multipleQuestionNo() ?
                    "Введите несколько правильных ответов, через пробел. Например, А В" :
                    "Соедините правильные ответы через пробел по паттерну №-Буква, например, 1-Б 2-В 3-Д");
        }
        System.out.printf("> ");

        String answer = inputReader.nextLine().toUpperCase();

        if (this.isCorrectAnswer(answer, number)) {
            System.out.println(this.correctAnswerReply.
                    get(new Random().nextInt(correctAnswerReply.size())));
            this.correctAnswersCount += 1;
        } else {
            System.out.println(wrongAnswerReply.get(new Random().nextInt(wrongAnswerReply.size())));
        }
        System.out.println("Правильный ответ: " + this.correctAnswers.get(number));
        System.out.println();
    }
//
//        this.numOfQuestions = Integer.valueOf(numOfQuestions);
//        questionsNumbers = questionsNumbers.subList(0, this.numOfQuestions);
//        Collections.shuffle(questionsNumbers);
//
//        while (currentQuestionNum < this.numOfQuestions) {
//            if (testEnded)
//                break;
//            this.nextQuestion();
//        }
//
//        getResults();
//    }
//
//    private void nextQuestion() {
//        Integer questionNo = questionsNumbers.get(currentQuestionNum);
//        currentQuestionNum += 1;
//        try {
//            this.printQuestion(questionNo);
//            this.askAnswer(questionNo);
//        } catch (NoDataException noData) {
//        }
//    }

//    public void printQuestion(int number) throws NoDataException {
//        //questionNo = 25;
//        String question = questions.get(number);
//        if (question.length() <= 3) {
//            question = ":";
//        }
//        System.out.println(Вопрос " + (currentQuestionNum) + " " + question);
//        variants.get(number)
//        answers.get(number).forEach((listItem) -> {
//            listItem.forEach((key, value) -> {
//                System.out.println(key + ": " + value);
//            });
//        });
//    }

    protected void getResults() {
        testEnded = true;
        System.out.println("На этом Все! Подведем итоги..");
        System.out.println("Правильных ответов: " + this.correctAnswersCount + " из " + numOfQuestions);

        if (this.byTime) {
            System.out.println("Увы, время кончилось.");
            System.out.println("Поэтому ответы не засчитываются =*С");
        } else if (this.correctAnswersCount == numOfQuestions) {
            System.out.println("Так держать! Все отвечено верно! Можешь смело сдавать тест в бою!");
        } else if (this.correctAnswersCount >= (int) (numOfQuestions * 0.8)) {
            System.out.println("Весьма неплохо! Знаешь почти все, но немного доучить не помешает");
        } else if (this.correctAnswersCount >= (int) (numOfQuestions * 0.6)) {
            System.out.println("Ты была близка к победе. Но что-то пошло не так. Советую доучить");
        } else if (this.correctAnswersCount >= (int) (numOfQuestions * 0.4)) {
            System.out.println("ЭТО НИКУДА НЕ ГОДИТСЯ. Садись и учи. Плохо");
        } else if (this.correctAnswersCount >= 0) {
            System.out.println("Слушай. Это фиаско, братан. Пожалуй надо писать заявление об уходе...");
        }
        //showMenu();
    }
//
//    public void printQuestionWithAnswer(int number) throws NoDataException {
//        if (number > questions.size()) throw new NoDataException("Нет такого вопроса");
//
//        this.printQuestion(number);
//        this.printCorrectAnswerFor(number);
//
//    }

//    public void printQuestion(int number) throws NoDataException {
//        //questionNo = 25;
//        String question = questions.get(number);
//        if (question.length() <= 3) {
//            question = ":";
//        }
//
//        if (number > variants.size()) throw new NoDataException("Нет такого вопроса");
//        //System.out.println("Вопрос " + (this.numOfQuestions - questionsNumbers.size()) + " " + question);
//        System.out.println("Вопрос " + (currentQuestionNum) + " " + question);
//        variants.get(number).forEach((listItem) -> {
//            listItem.forEach((key, value) -> {
//                System.out.println(key + ": " + value);
//            });
//        });
//    }

    public void showMenu() {
        String answer = "-1";

        while (Integer.valueOf(answer) != 3) {
            System.out.println("Вот так вот. Что делаем дальше?");
            System.out.println("1. Повторить тест");
            //System.out.println("2. Проверить вопрос");
            System.out.println("2. Выйти");
            System.out.print("Введите номер следующего действия> ");

            answer = inputReader.nextLine();

            while (!answer.matches("[1-2]")) {
                System.out.println("Введите число от 1 до 2");
                answer = inputReader.nextLine();
            }

            switch (Integer.valueOf(answer)) {
                case 1:
                    reset();
                    start();
                    break;
                case 2:

//                case 2:
//                    System.out.print("Какой вопрос проверить?\n> ");
//                    String num = inputReader.nextLine();
//                    while (!num.matches("[1-9]+")) {
//                        System.out.println("Введите номер вопроса.");
//                        num = inputReader.nextLine();
//                    }
//                    if (this.numOfQuestions < Integer.valueOf(num)) {
//                        System.out.println("Такого вопроса не было в списке. Не бузи мне тут");
//                        break;
//                    }
//                    try {
//                        currentQuestionNum = Integer.valueOf(num);
                    //this.printQuestionWithAnswer(questionsNumbers.get(Integer.valueOf(num) - 1));
//                    } catch (NoDataException noDataEx) {
//                        System.out.println(noDataEx.getMessage());
            }
            break;
        }
//        }
        System.out.println("Желаю удачи в подготовке! Пока-пока!");
        inputReader.close();
        inputReader = null;
    }
}