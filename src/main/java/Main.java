import quiz.FermentiQuiz;

public class Main {
    public static void main(String[] args) {
        FermentiQuiz quiz = new FermentiQuiz();

        quiz.prepareData();

        quiz.start();
    }
}
